(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.bigbigshake = function() {
	this.initialize(img.bigbigshake);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,175,315);


(lib.bigshake1 = function() {
	this.initialize(img.bigshake1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,274);


(lib.bigshake2 = function() {
	this.initialize(img.bigshake2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,284);


(lib.bigshake3 = function() {
	this.initialize(img.bigshake3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,146,263);


(lib.bigshake4 = function() {
	this.initialize(img.bigshake4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,179,279);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,83);


(lib.nut = function() {
	this.initialize(img.nut);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,105,71);


(lib.puppet = function() {
	this.initialize(img.puppet);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,220);


(lib.shake1 = function() {
	this.initialize(img.shake1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,94,167);


(lib.shake10 = function() {
	this.initialize(img.shake10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,115,199);


(lib.shake2 = function() {
	this.initialize(img.shake2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,195,348);


(lib.shake3 = function() {
	this.initialize(img.shake3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,101,193);


(lib.shake4 = function() {
	this.initialize(img.shake4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,73,112);


(lib.shake5 = function() {
	this.initialize(img.shake5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,101,112);


(lib.shake6 = function() {
	this.initialize(img.shake6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,99);


(lib.shake7 = function() {
	this.initialize(img.shake7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,172,321);


(lib.shake8 = function() {
	this.initialize(img.shake8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,132,206);


(lib.shake9 = function() {
	this.initialize(img.shake9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,47,91);


(lib.strawberry = function() {
	this.initialize(img.strawberry);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,106,60);


(lib.vanilla = function() {
	this.initialize(img.vanilla);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,114,52);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.wing11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake10();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5026,0.5025);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing11, new cjs.Rectangle(0,0,57.8,100), null);


(lib.wing9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake9();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4947,0.4945);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing9, new cjs.Rectangle(0,0,23.3,45), null);


(lib.wing8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake6();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5052,0.5051);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing8, new cjs.Rectangle(0,0,29.3,50), null);


(lib.wing7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake8();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4856,0.4854);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing7, new cjs.Rectangle(0,0,64.1,100), null);


(lib.wing6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake5();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4465,0.4464);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing6, new cjs.Rectangle(0,0,45.1,50), null);


(lib.wing5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake7();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4671,0.4673);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing5, new cjs.Rectangle(0,0,80.4,150), null);


(lib.wing4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4597,0.4598);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing4, new cjs.Rectangle(0,0,89.7,160), null);


(lib.wing3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4787,0.4787);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing3, new cjs.Rectangle(0,0,45,80), null);


(lib.wing2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5356,0.5357);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing2, new cjs.Rectangle(0,0,39.1,60), null);


(lib.wing1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.shake3();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5183,0.5181);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.wing1, new cjs.Rectangle(0,0,52.4,100), null);


(lib.w1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigshake4();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4911,0.491);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.w1MC, new cjs.Rectangle(0,0,87.9,137), null);


(lib.vanillaMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.vanilla();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4807,0.4808);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.vanillaMC, new cjs.Rectangle(0,0,54.8,25), null);


(lib.v4MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhMCcQgdgagCgsIBRgYQABArAXAAQAWAAABgxQgBgtgVAAQgNAAgHATIhFgDIAHjQICwAAIAABOIhmAAIgDA9QAOgOAYAAQAmAAAWAeQAWAdAAA0QAAA6gfAjQgfAigxAAQgtAAgcgag");
	this.shape.setTransform(98.55,37.275);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhMCkQgZgVgIglIBEgfQAEAhAYAAQASAAALgXQAKgYAAgpIAAgCIAAAAQgNAlgmAAQgnAAgYgfQgYghAAgxQAAg4AegiQAegjAvAAQA9AAAeA3QAbAvAABJQAABbgiAzQghAyg7ABQgmgBgZgUgAgbg+QAAAvAXgBQAXABAAgvQAAgtgXgBQgXABAAAtg");
	this.shape_1.setTransform(75.425,37.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgpArQApgBAAgcIAAgEIgKACQgSAAgLgMQgMgLAAgTQAAgWAOgNQANgOAVAAQAYAAAQASQAPASAAAdQAAArgaAZQgaAagpAAg");
	this.shape_2.setTransform(57.275,53.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhnC2IAAhCIA2g9IAVgaQAvg5AAgrQAAgfgVAAQgVAAAAAdQAAAOAKANIhJAjQgVgaAAgqQAAgsAdgdQAegdAwAAQAyAAAdAdQAdAeAAAyQAAApgVAmQgWApg6A7IBlAAIAABLg");
	this.shape_3.setTransform(39.525,36.85);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhgA4IgXAAIAAgwIAUAAIAAgIIAAgLIgUAAIAAgvIAYAAQARh+BfABQAvAAAaAeQAbAdADA4IhOAUQAAg6gZgBQgXAAgFAxIAkAAIgDAvIgkAAIAAALIAAAIIAiAAIgEAwIgcAAQAGA0AXgBQAbABgBg9IBNAVQgCA3gcAfQgaAfguAAQhhgBgRiAg");
	this.shape_4.setTransform(15.075,37.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v4MC, new cjs.Rectangle(0,0,112.3,71.3), null);


(lib.v3MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhMCcQgdgagCgsIBRgYQABArAXAAQAXAAAAgxQgBgtgVAAQgOAAgFATIhGgDIAHjQICwAAIAABOIhmAAIgCA9QANgOAYAAQAnAAAWAeQAVAdAAA0QAAA6gfAjQgeAigxAAQguAAgcgag");
	this.shape.setTransform(74.05,37.275);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgCC0IAAhPIhyAAIAAhEIBujUIBYAAIAADRIAjAAIAABHIgjAAIAABPgAgvAcIAAACIAtAAIAAhYIgDAAg");
	this.shape_1.setTransform(50.625,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgpArQApgBAAgcIAAgEIgKACQgSAAgLgMQgMgLAAgTQAAgWAOgNQANgOAVAAQAYAAAQASQAPASAAAdQAAArgaAZQgaAagpAAg");
	this.shape_2.setTransform(32.275,53.15);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhVCBQgbgvAAhJQAAhbAigzQAhgyA7AAQAlgBAaAVQAZAWAIAlIhEAfQgEgigYAAQgSAAgLAYQgKAWAAAqIAAABIAAAAQANgkAmAAQAnAAAYAhQAYAeAAAzQAAA4geAiQgeAjgvAAQg9AAgeg4gAgSA/QAAAuAWAAQAYAAAAguQAAgugYAAQgWAAAAAug");
	this.shape_3.setTransform(14.075,37.05);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v3MC, new cjs.Rectangle(0,0,87.8,71.3), null);


(lib.v2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAaC0QgIgOgCgZIgGhAQgEgagSAAIgRAAIAACBIhcAAIAAlnIB5AAQByAAAABqQAAAggQAYQgPAWgaAHIAAABQAmAKAFAsIAJBJQADAXAKARgAgdgTIATAAQAPAAAHgJQAGgKAAgYQAAgYgGgJQgHgJgPAAIgTAAg");
	this.shape.setTransform(95.65,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhcCJQgggvgBhaQABhYAggwQAhgvA7AAQA+AAAfAvQAhAwgBBYQABBaghAvQgfAwg+AAQg8AAgggwgAgVhSQgIAYABA6QgBA7AIAYQAGAZAPgBQARAAAGgYQAHgXgBg8QABg7gHgXQgGgZgRAAQgPABgGAYg");
	this.shape_1.setTransform(68.45,37.05);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhcCJQghgvAAhaQAAhYAhgwQAhgvA7AAQA9AAAhAvQAfAwAABYQAABagfAvQghAwg9AAQg8AAgggwgAgVhSQgIAYABA6QgBA7AIAYQAGAZAPgBQARAAAGgYQAHgXAAg8QAAg7gHgXQgGgZgRAAQgPABgGAYg");
	this.shape_2.setTransform(41.65,37.05);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgvC0IhSlnIBhAAIAhDWIABAAIAijWIBeAAIhQFng");
	this.shape_3.setTransform(15.125,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v2MC, new cjs.Rectangle(0,0,110.3,71.3), null);


(lib.v1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAlC0IhCitIgCAAIAACtIhVAAIAAlnIBVAAIA+CpIACAAIAAipIBUAAIAAFng");
	this.shape.setTransform(67.625,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAnC0IgLhHIg6AAIgLBHIhXAAIBHlnIBzAAIBHFngAASAoIgSh1IgBAAIgSB1IAlAAg");
	this.shape_1.setTransform(41.275,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgvC0IhSlnIBhAAIAhDWIABAAIAijWIBeAAIhQFng");
	this.shape_2.setTransform(15.125,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.v1MC, new cjs.Rectangle(0,0,83.2,71.3), null);


(lib.toplinemc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AJYBLIAAiVIGQAAIAACVgAjHBLIAAiVIGPAAIAACVgAvnBLIAAiVIGQAAIAACVg");
	this.shape.setTransform(100,7.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.toplinemc, new cjs.Rectangle(0,0,200,15), null);


(lib.t3mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AguC0IAAkbIhBAAIAAhMIDfAAIAABMIhBAAIAAEbg");
	this.shape.setTransform(88.675,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AAnC0IgLhHIg6AAIgLBHIhXAAIBHlnIBzAAIBHFngAASAoIgSh1IgBAAIgSB1IAlAAg");
	this.shape_1.setTransform(63.775,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("AhcC0IAAlnIC5AAIAABMIheAAIAABCIBQAAIAABHIhQAAIAABGIBeAAIAABMg");
	this.shape_2.setTransform(40.225,37.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E4002B").s().p("AAbC0IAAiRIg1AAIAACRIheAAIAAlnIBeAAIAACMIA1AAIAAiMIBeAAIAAFng");
	this.shape_3.setTransform(15.65,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.t3mc, new cjs.Rectangle(0,0,102.8,71.3), null);


(lib.t2mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AhcC0IAAlnIC5AAIAABMIheAAIAABCIBQAAIAABHIhQAAIAABGIBeAAIAABMg");
	this.shape.setTransform(63.975,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AAbC0IAAiRIg1AAIAACRIhdAAIAAlnIBdAAIAACMIA1AAIAAiMIBeAAIAAFng");
	this.shape_1.setTransform(39.4,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("AguC0IAAkbIhBAAIAAhMIDfAAIAABMIhBAAIAAEbg");
	this.shape_2.setTransform(13.875,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.t2mc, new cjs.Rectangle(0,0,76.7,71.3), null);


(lib.t1mc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AguC0IAAkbIhBAAIAAhMIDfAAIAABMIhBAAIAAEbg");
	this.shape.setTransform(86.125,37.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AAnC0IgLhHIg6AAIgLBHIhXAAIBHlnIBzAAIBHFngAASAoIgSh1IgBAAIgSB1IAlAAg");
	this.shape_1.setTransform(61.225,37.075);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("AhcC0IAAlnIC5AAIAABMIheAAIAABCIBQAAIAABHIhQAAIAABGIBeAAIAABMg");
	this.shape_2.setTransform(37.675,37.075);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E4002B").s().p("AhwC0IAAlnIBxAAQAyAAAaAYQAbAZAAAtQAABEgyAOIAAABQA7ANAABGQAABjhyAAgAgYBsIATAAQAZAAAAgnQAAgmgZAAIgTAAgAgYggIAQAAQAYAAAAgmQAAglgYAAIgQAAg");
	this.shape_3.setTransform(14.875,37.075);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.t1mc, new cjs.Rectangle(0,0,100.2,71.3), null);


(lib.strawberryBig = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.strawberry();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.strawberryBig, new cjs.Rectangle(0,0,53,30), null);


(lib.nutMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.nut();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4929,0.493);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.nutMC, new cjs.Rectangle(0,0,51.8,35), null);


(lib.legalMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgFAGQgDgCAAgEQAAgDADgCQACgDADAAQAEAAACADQADACAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape.setTransform(265.075,10.075);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgUAXQgGgJgBgNQABgOAHgJQAJgIAMAAQAJAAAHAEQAGAFADAHIgOAGQgDgJgIAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAEAAAEgCQADgDAAgEIAAgCIgLAAIAAgKIAaAAIAAAgIgLAAIgCgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_1.setTransform(260.55,7.85);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgEAMIgQAAIAXg7IAQAAIAVA7gAAHAGIgHgUIAAAAIgGAUIANAAg");
	this.shape_2.setTransform(254.35,7.85);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAIAJAAANQAAAOgIAIQgHAIgOAAgAgLASIAIAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgIAAg");
	this.shape_3.setTransform(248.3,7.85);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgPAbQgGgEgCgHIAMgFQABADADADQAEACADABQAEgBADgBQACgCAAgDQAAAAAAgBQAAAAAAgBQAAAAgBgBQAAAAgBAAIgIgEIgJgDIgFgCQgGgDAAgJQAAgIAGgGQAHgFAJAAQATAAAEAPIgOADQAAgDgDgBQgCgCgEAAQgDAAgCABQgBAAAAABQAAABgBAAQAAABAAAAQAAABAAAAQAAABAAABQAAAAAAABQABAAAAABQAAAAABABIAIACQALADADADQAGADABAJQgBAJgGAFQgHAFgLAAQgIAAgHgEg");
	this.shape_4.setTransform(242.35,7.85);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_5.setTransform(236.575,7.85);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgHAeIAAg7IAOAAIAAA7g");
	this.shape_6.setTransform(232,7.85);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAJAJgBANQABAOgJAIQgHAIgOAAgAgKASIAHAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgHAAg");
	this.shape_7.setTransform(227.75,7.85);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgXAeIAAg7IAYAAQALAAAGAGQAGAFAAAKQAAAIgHAFQgFAGgLAAIgJAAIAAATgAgIAAIAIAAQAIAAAAgIQAAgJgIAAIgIAAg");
	this.shape_8.setTransform(219.95,7.85);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgUAWQgIgIAAgOQAAgNAIgJQAHgIANAAQANAAAIAIQAIAJAAANQAAAOgIAIQgIAJgNAAQgNAAgHgJgAgIgNQgEAFAAAIQAAAJAEAEQADAGAFAAQAGAAAEgGQADgEAAgJQAAgIgDgFQgEgEgGAAQgFAAgDAEg");
	this.shape_9.setTransform(213.85,7.85);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_10.setTransform(205.475,7.85);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_11.setTransform(199.725,7.85);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_12.setTransform(194.475,7.85);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IAPAAIAAAvIAaAAIAAAMg");
	this.shape_13.setTransform(189.375,7.85);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IAPAAIAAAvIAaAAIAAAMg");
	this.shape_14.setTransform(184.325,7.85);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgFAMIgPAAIAXg7IAQAAIAVA7gAAHAGIgHgUIAAAAIgHAUIAOAAg");
	this.shape_15.setTransform(178.5,7.85);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_16.setTransform(170.125,7.85);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_17.setTransform(164.375,7.85);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgFAGQgDgCAAgEQAAgDADgCQACgDADAAQAEAAACADQADACAAADQAAAEgDACQgCADgEAAQgDAAgCgDg");
	this.shape_18.setTransform(158.425,10.075);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgUAXQgGgJgBgNQABgOAHgJQAJgIAMAAQAJAAAHAEQAGAFADAHIgOAGQgDgJgIAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAEAAAEgCQADgDAAgEIAAgCIgLAAIAAgKIAaAAIAAAgIgLAAIgCgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_19.setTransform(153.9,7.85);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgGAeIAAg7IANAAIAAA7g");
	this.shape_20.setTransform(149.5,7.85);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAIAJAAANQAAAOgIAIQgHAIgOAAgAgLASIAIAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgIAAg");
	this.shape_21.setTransform(145.25,7.85);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IAPAAIAAAvIAaAAIAAAMg");
	this.shape_22.setTransform(139.625,7.85);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_23.setTransform(134.425,7.85);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgUAXQgGgJAAgNQAAgOAIgJQAHgIANAAQAJAAAHAEQAHAFACAHIgPAGQgBgJgJAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAFAAADgCQACgDAAgEIAAgCIgKAAIAAgKIAaAAIAAAgIgKAAIgDgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_24.setTransform(128.45,7.85);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgUAXQgGgJgBgNQABgOAHgJQAJgIAMAAQAJAAAHAEQAGAFADAHIgOAGQgDgJgIAAQgGAAgDAEQgEAGAAAIQAAAIAEAFQADAFAGAAQAEAAAEgCQADgDAAgEIAAgCIgLAAIAAgKIAaAAIAAAgIgLAAIgCgHQgBADgEADQgFACgFAAQgMAAgIgIg");
	this.shape_25.setTransform(120.15,7.85);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgGAeIAAg7IANAAIAAA7g");
	this.shape_26.setTransform(115.75,7.85);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAJAeIgJgmIAAAAIgIAmIgPAAIgPg7IAPAAIAJAoIAAAAIAJgoIAMAAIAJAoIAAAAIAJgoIAOAAIgOA7g");
	this.shape_27.setTransform(110.375,7.85);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgSAYQgGgGAAgMIAAgjIAPAAIAAAiQAAANAJAAQAKAAAAgNIAAgiIAPAAIAAAjQAAAMgGAGQgHAGgMABQgLgBgHgGg");
	this.shape_28.setTransform(103.325,7.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_29.setTransform(97.775,7.85);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_30.setTransform(92.525,7.85);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgHAeIAAguIgTAAIAAgNIA0AAIAAANIgTAAIAAAug");
	this.shape_31.setTransform(84.95,7.85);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgUAeIAAg7IApAAIAAANIgbAAIAAALIAYAAIAAAKIgYAAIAAANIAbAAIAAAMg");
	this.shape_32.setTransform(79.525,7.85);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgHAeIAAg7IAPAAIAAA7g");
	this.shape_33.setTransform(75.45,7.85);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_34.setTransform(70.875,7.85);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgPAbQgHgEgCgHIAOgFQAAADADADQAEACADABQAEgBADgBQACgCABgDQgBAAAAgBQAAAAAAgBQAAAAgBgBQAAAAgBAAIgIgEIgJgDIgFgCQgHgDAAgJQAAgIAHgGQAHgFAJAAQAUAAADAPIgOADQAAgDgCgBQgDgCgEAAQgDAAgCABQAAAAgBABQAAABAAAAQgBABAAAAQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABQABAAAAABIAIACQALADADADQAHADAAAJQAAAJgHAFQgHAFgLAAQgJAAgGgEg");
	this.shape_35.setTransform(62.95,7.85);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgGAeIAAg7IANAAIAAA7g");
	this.shape_36.setTransform(59.05,7.85);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgaAeIAAg7IAXAAQAOAAAHAHQAJAJgBANQABAOgJAIQgHAIgOAAgAgKASIAHAAQAGAAAEgFQAEgFAAgIQAAgQgOAAIgHAAg");
	this.shape_37.setTransform(52.8,7.85);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgUAWQgIgIAAgOQAAgNAIgJQAHgIANAAQANAAAIAIQAIAJAAANQAAAOgIAIQgIAJgNAAQgNAAgHgJgAgIgNQgEAFAAAIQAAAJAEAEQADAGAFAAQAGAAAEgGQADgEAAgJQAAgIgDgFQgEgEgGAAQgFAAgDAEg");
	this.shape_38.setTransform(46.45,7.85);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgXAeIAAg7IAaAAQAJAAAFAEQAFAFAAAHQAAAFgDAEQgDADgFABIAAAAQAGAAAEAEQADAEAAAFQAAAIgFAFQgGAEgKAAgAgJATIAJAAQAJAAAAgHQAAgIgJAAIgJAAgAgJgEIAJAAQAHABAAgIQAAgHgHAAIgJAAg");
	this.shape_39.setTransform(40.675,7.85);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AANAeIgYgkIAAAkIgOAAIAAg7IAOAAIAXAkIAAgkIAOAAIAAA7g");
	this.shape_40.setTransform(34.425,7.85);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgFAMIgOAAIAVg7IAQAAIAXA7gAAHAGIgHgUIAAAAIgHAUIAOAAg");
	this.shape_41.setTransform(28.05,7.85);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAPAeIgEgMIgVAAIgFAMIgOAAIAVg7IAQAAIAXA7gAAHAGIgHgUIAAAAIgHAUIAOAAg");
	this.shape_42.setTransform(21.75,7.85);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgHAeIAAguIgTAAIAAgNIA0AAIAAANIgTAAIAAAug");
	this.shape_43.setTransform(13.75,7.85);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgHAeIAAg7IAPAAIAAA7g");
	this.shape_44.setTransform(9.5,7.85);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgZAeIAAg7IAWAAQANAAAJAHQAHAJABANQgBAOgHAIQgJAIgNAAgAgKASIAGAAQAHAAAEgFQAEgFAAgIQAAgQgPAAIgGAAg");
	this.shape_45.setTransform(5.25,7.85);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.legalMC, new cjs.Rectangle(0,0,268.5,15.2), null);


(lib.l2MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag+B/QgXgVgCgkIBCgTQABAiATAAQARAAABgnQgBglgQAAQgMAAgFAPIg4gCIAGiqICPAAIAABAIhTAAIgCAyQALgMATAAQAfAAATAZQARAXAAArQAAAvgZAdQgZAcgoAAQglAAgXgWg");
	this.shape.setTransform(118.75,44.725);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag+CFQgUgQgHgfIA4gZQADAbAUAAQAOAAAJgTQAIgTAAghIAAgCIAAAAQgLAegeAAQggAAgTgaQgUgaAAgpQAAgsAYgdQAZgdAmABQAyAAAYAtQAWAmAAA8QAABKgbApQgcApgwAAQgeABgVgSgAgWgyQAAAlATABQATgBAAglQAAglgTAAQgTAAAAAlg");
	this.shape_1.setTransform(99.875,44.55);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AhDBFQBDgCAAguIgBgFIgPADQgdAAgTgTQgTgSAAggQABgiAWgWQAWgWAhAAQAoAAAYAdQAZAdAAAvQgBBFgpApQgpAqhEAAg");
	this.shape_2.setTransform(91,84.375);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AinEmIAAhsIBXhhIAjgqQBLhdAAhEQAAgzgiAAQgiAAAAAuQAAAXAQAWIh0A4QgjgrAAhCQAAhIAvguQAwgwBOAAQBQAAAvAvQAvAwAABRQAABCgiA+QgjBChdBeICiAAIAAB7g");
	this.shape_3.setTransform(62.475,58.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AibBaIgmAAIAAhNIAhAAIAAgNIAAgTIghAAIAAhNIAnAAQAcjJCaABQBLgBArAxQAqAwAFBaIh9AgQAAhegoAAQgmAAgJBMIA8AAIgGBNIg6AAIAAATIAAANIA3AAIgGBNIguAAQAKBTAmAAQAqABgBhiIB9AgQgEBagsAxQgsAyhJAAQicAAgcjPg");
	this.shape_4.setTransform(23.05,58.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.l2MC, new cjs.Rectangle(0,0,130.3,112.4), null);


(lib.l1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBFQgNgLgBgUIAkgKQABATAKAAQAJAAAAgWQAAgUgJAAQgGAAgCAIIgggBIAEhcIBNAAIAAAjIgtAAIgBAbQAGgGAKAAQARAAAKANQAKAMAAAYQAAAZgOAQQgOAPgUAAQgVAAgMgMg");
	this.shape.setTransform(33.95,17.675);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgBBQIAAgjIgyAAIAAgeIAxheIAmAAIAABdIAQAAIAAAfIgQAAIAAAjgAgVANIAAABIAUAAIAAgnIgBAAg");
	this.shape_1.setTransform(23.575,17.575);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSATQASAAAAgNIAAgCIgEABQgIAAgFgFQgFgEAAgJQAAgJAGgHQAGgFAJAAQALAAAGAHQAHAJAAANQAAARgMAMQgLAMgSgBg");
	this.shape_2.setTransform(15.45,24.75);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AglA5QgMgVAAghQAAgnAPgXQAPgWAaAAQAQAAAMAJQAKAJAFARIgfANQgCgOgKAAQgIAAgFAKQgEALAAARIAAABQAFgQARAAQARAAALAPQAKANAAAWQAAAZgNAPQgNAQgWAAQgaAAgNgZgAgIAbQAAAVAKAAQALAAAAgVQAAgUgLAAQgKAAAAAUg");
	this.shape_3.setTransform(7.35,17.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.l1MC, new cjs.Rectangle(0,0,41.3,33.9), null);


(lib.kfclogo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.kfclogo, new cjs.Rectangle(0,0,147,83), null);


(lib.friesMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigshake2();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.friesMC, new cjs.Rectangle(0,0,73.5,142), null);


(lib.fasirtMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigshake1();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.fasirtMC, new cjs.Rectangle(0,0,71.5,137), null);


(lib.d1MC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhTClQgggegDgyIBTgYQAAAZAJAQQAJAPAQAAQAXAAAAgYQAAgMgKgMQgJgLgfgWQgzghgRgZQgQgbAAghQAAgyAggfQAhgfA0AAQAyAAAeAcQAdAcACAzIhTASQgBgxgbAAQgWAAAAAZQAAALAJAKQAIAKAeAUQAsAdASAWQAbAhAAArQAAAwgiAgQgiAegzAAQgzAAgggeg");
	this.shape.setTransform(239.725,39.025);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhiC+IAAl7IDEAAIAABQIhjAAIAABGIBUAAIAABLIhUAAIAABKIBjAAIAABQg");
	this.shape_1.setTransform(216.6,39.025);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAaC+IguiXIgLAAIAACXIhjAAIAAl7IBjAAIAACSIALAAIAqiSIBoAAIg8C5IBBDCg");
	this.shape_2.setTransform(192.025,39.025);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AApC+IgLhLIg+AAIgLBLIhcAAIBLl7IB5AAIBLF7gAATAqIgTh8IgCAAIgSB8IAnAAg");
	this.shape_3.setTransform(163.45,39.025);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAdC+IAAiZIg4AAIAACZIhjAAIAAl7IBjAAIAACUIA4AAIAAiUIBiAAIAAF7g");
	this.shape_4.setTransform(135.325,39.025);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhTClQgggegDgyIBTgYQAAAZAJAQQAJAPAQAAQAXAAAAgYQAAgMgKgMQgJgLgfgWQgzghgRgZQgQgbAAghQAAgyAggfQAhgfA0AAQAyAAAeAcQAdAcACAzIhTASQgBgxgbAAQgWAAAAAZQAAALAJAKQAIAKAeAUQAsAdASAWQAbAhAAArQAAAwgiAgQgiAegzAAQgzAAgggeg");
	this.shape_5.setTransform(108.575,39.025);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAaC+IguiXIgLAAIAACXIhjAAIAAl7IBjAAIAACSIALAAIAqiSIBoAAIg8C5IBBDCg");
	this.shape_6.setTransform(83.525,39.025);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AheC+IAAl7IBiAAIAAErIBbAAIAABQg");
	this.shape_7.setTransform(58.475,39.025);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgwC+IAAl7IBiAAIAAF7g");
	this.shape_8.setTransform(40.65,39.025);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AA0C+IAAiKIABhHIgCAAIgeByIgqAAIgdhyIgCAAIABBHIAACKIhbAAIAAl7IBlAAIApCLIACAAIApiLIBkAAIAAF7g");
	this.shape_9.setTransform(18.025,39.025);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.d1MC, new cjs.Rectangle(0,0,254.3,75.1), null);


(lib.cta = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.pina = function(){
			
			TweenMax.killTweensOf(mask);
			mask.x = -60;
			TweenMax
			.to(mask, 1.5, {
		        x: 205,
		        ease: Circ.easeOut,
				repeat:2,
		        delay: 0
		    });
			}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// Layer_3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApXCvIAAldISvAAIAAFdg");
	mask.setTransform(-60,17.5);

	// Layer_4
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E4002B").s().p("AgXAfQgLgMAAgTQAAgTALgKQAJgLARAAQAMgBAIAGQAJAFADAKIgSAJQgDgMgLAAQgHgBgFAHQgEAGAAALQAAALAEAHQAFAGAHAAQALAAADgMIASAIQgDAKgJAGQgIAFgMABQgRgBgJgKg");
	this.shape.setTransform(103.4,17.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("AgbAoIAAhPIA3AAIAAARIgjAAIAAASIAfAAIAAAPIgfAAIAAAdg");
	this.shape_1.setTransform(96.375,17.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#E4002B").s().p("AAMAoIgVggIgGAAIAAAgIgUAAIAAhPIAUAAIAAAfIAHAAIATgfIAXAAIgaAnIAcAog");
	this.shape_2.setTransform(89.175,17.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E4002B").s().p("AAMAoIgMg0IAAAAIgLA0IgVAAIgThPIAUAAIAMA1IALg1IARAAIAMA1IAMg1IATAAIgTBPg");
	this.shape_3.setTransform(76.775,17.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#E4002B").s().p("AgYAgQgJgIAAgQIAAgwIAUAAIAAAvQAAARANAAQANAAAAgRIAAgvIAVAAIAAAwQAAAQgJAIQgJAJgQAAQgPAAgJgJg");
	this.shape_4.setTransform(67.375,17.875);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#E4002B").s().p("AgbAfQgKgMAAgTQAAgSAKgLQALgLAQAAQASAAAKALQAKALAAASQAAATgKAMQgKAKgSABQgQgBgLgKgAgMgRQgEAGAAALQAAALAEAHQAFAGAHAAQAIAAAFgGQAFgHAAgLQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_5.setTransform(59.125,17.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E4002B").s().p("AgTAiQgIgGgCgMIATgGQAAAOAKAAQAKAAAAgMIAAg0IAUAAIAAAzQAAAOgIAIQgIAIgOAAQgLAAgIgHg");
	this.shape_6.setTransform(51.275,17.875);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#E4002B").s().p("AgjAoIAAhPIAfAAQASAAALAKQALAKgBATQABATgLALQgLAKgSAAgAgPAXIAJAAQAJAAAGgGQAFgGABgLQAAgWgVAAIgJAAg");
	this.shape_7.setTransform(41.55,17.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#E4002B").s().p("AARAoIgggxIAAAAIAAAxIgTAAIAAhPIATAAIAfAwIAAAAIAAgwIATAAIAABPg");
	this.shape_8.setTransform(32.9,17.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E4002B").s().p("AgJAoIAAhPIATAAIAABPg");
	this.shape_9.setTransform(26.8,17.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#E4002B").s().p("AgKAoIgchPIAVAAIARA3IABAAIARg3IAVAAIgcBPg");
	this.shape_10.setTransform(21.025,17.8);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2,this.shape_3,this.shape_4,this.shape_5,this.shape_6,this.shape_7,this.shape_8,this.shape_9,this.shape_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer_2
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("ApNClIAAlJISbAAIAAFJg");
	this.shape_11.setTransform(60.0172,17.5106,1.017,1.0606);

	var maskedShapeInstanceList = [this.shape_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

	// Layer_5
	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgXAfQgLgMAAgTQAAgTALgKQAJgLARAAQAMgBAIAGQAJAFADAKIgSAJQgDgMgLAAQgHgBgFAHQgEAGAAALQAAALAEAHQAFAGAHAAQALAAADgMIASAIQgDAKgJAGQgIAFgMABQgRgBgJgKg");
	this.shape_12.setTransform(103.4,17.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgbAoIAAhPIA3AAIAAARIgjAAIAAASIAfAAIAAAPIgfAAIAAAdg");
	this.shape_13.setTransform(96.375,17.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAMAoIgVggIgGAAIAAAgIgUAAIAAhPIAUAAIAAAfIAHAAIATgfIAXAAIgaAnIAcAog");
	this.shape_14.setTransform(89.175,17.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAMAoIgMg0IAAAAIgLA0IgVAAIgThPIAUAAIAMA1IALg1IARAAIAMA1IAMg1IATAAIgTBPg");
	this.shape_15.setTransform(76.775,17.8);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgYAgQgJgIAAgQIAAgwIAUAAIAAAvQAAARANAAQANAAAAgRIAAgvIAVAAIAAAwQAAAQgJAIQgJAJgQAAQgPAAgJgJg");
	this.shape_16.setTransform(67.375,17.875);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgbAfQgKgMAAgTQAAgSAKgLQALgLAQAAQASAAAKALQAKALAAASQAAATgKAMQgKAKgSABQgQgBgLgKgAgMgRQgEAGAAALQAAALAEAHQAFAGAHAAQAIAAAFgGQAFgHAAgLQAAgLgFgGQgFgHgIAAQgHAAgFAHg");
	this.shape_17.setTransform(59.125,17.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgTAiQgIgGgCgMIATgGQAAAOAKAAQAKAAAAgMIAAg0IAUAAIAAAzQAAAOgIAIQgIAIgOAAQgLAAgIgHg");
	this.shape_18.setTransform(51.275,17.875);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgjAoIAAhPIAfAAQASAAALAKQALAKgBATQABATgLALQgLAKgSAAgAgPAXIAJAAQAJAAAGgGQAFgGABgLQAAgWgVAAIgJAAg");
	this.shape_19.setTransform(41.55,17.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AARAoIgggxIAAAAIAAAxIgTAAIAAhPIATAAIAfAwIAAAAIAAgwIATAAIAABPg");
	this.shape_20.setTransform(32.9,17.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgJAoIAAhPIATAAIAABPg");
	this.shape_21.setTransform(26.8,17.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgKAoIgchPIAVAAIARA3IABAAIARg3IAVAAIgcBPg");
	this.shape_22.setTransform(21.025,17.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12}]}).wait(1));

	// Layer_1
	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#FFFFFF").ss(2,1,1).p("ApXiuISvAAIAAFdIyvAAg");
	this.shape_23.setTransform(60,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_23).wait(1));

}).prototype = getMCSymbolPrototype(lib.cta, new cjs.Rectangle(-1,-1,122,37), null);


(lib.crossline2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AjCAUIAAgnIGFAAIAAAng");
	this.shape.setTransform(19.525,2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.crossline2, new cjs.Rectangle(0,0,39.1,4), null);


(lib.crossline = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AnLAoIAAhPIOXAAIAABPg");
	this.shape.setTransform(45.975,2.9999,1,0.75);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.crossline, new cjs.Rectangle(0,0,92,6), null);


(lib.burgerMC = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigshake3();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.502,0.5019);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.burgerMC, new cjs.Rectangle(0,0,73.3,132), null);


(lib.bigbigshake_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bigbigshake();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.4983,0.4984);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.bigbigshake_1, new cjs.Rectangle(0,0,87.2,157), null);


(lib.redBG = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_4
	this.crossline = new lib.crossline();
	this.crossline.name = "crossline";
	this.crossline.parent = this;
	this.crossline.setTransform(95.1,358.8,1,1.0021,0,0,0,0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.crossline).wait(1));

	// Layer_3
	this.v4MC = new lib.v4MC();
	this.v4MC.name = "v4MC";
	this.v4MC.parent = this;
	this.v4MC.setTransform(389.15,406.85,1,1,0,0,0,55.9,35.6);

	this.v2MC = new lib.v3MC();
	this.v2MC.name = "v2MC";
	this.v2MC.parent = this;
	this.v2MC.setTransform(375.1,360.85,1,1,0,0,0,43.9,35.6);

	this.v3MC = new lib.v2MC();
	this.v3MC.name = "v3MC";
	this.v3MC.parent = this;
	this.v3MC.setTransform(-16.5,406.85,1,1,0,0,0,113.5,35.6);

	this.v1MC = new lib.v1MC();
	this.v1MC.name = "v1MC";
	this.v1MC.parent = this;
	this.v1MC.setTransform(-12,360.85,1,1,0,0,0,88,35.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.v1MC},{t:this.v3MC},{t:this.v2MC},{t:this.v4MC}]}).wait(1));

	// Layer_2
	this.instance = new lib.puppet();
	this.instance.parent = this;
	this.instance.setTransform(42,451,0.5,0.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EAEsBFnMAAAhGTIDIAAMAAABGTgEgBjBFnMAAAhGTIDHAAMAAABGTgEgHzBFnMAAAhGTIDIAAMAAABGTgAEs+hMAAAgnFIDIAAMAAAAnFgAhj+hMAAAgnFIDHAAMAAAAnFgAnz+hMAAAgnFIDIAAMAAAAnFg");
	this.shape.setTransform(70,445.45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape},{t:this.instance}]}).wait(1));

	// Layer_1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E4002B").s().p("EgY/BV8MAAAir3MAx/AAAMAAACr3g");
	this.shape_1.setTransform(160,550);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = getMCSymbolPrototype(lib.redBG, new cjs.Rectangle(-130,0,575.5,1100), null);


// stage content:
(lib.kfc_320x240 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var d = 0.5;
		var self = this;
		var stageHeight = 250;
		var stageWidth = 300;
		var offsetX = 200;
		
		TweenMax.delayedCall(d, animateInFrame1);
		
		
		function animateInFrame1() {
		
		
		
		    var d = 0;
		    var wingToY = [40, 30, 13, 20, 220, 100, 198, 50, 180, 200];
		    var wingToYBack = -stageHeight;
		
		    self.t1MC.y = self.t2MC.y = self.t3MC.y = self.toplineMC.y = -stageHeight;
		    self.redBG.y = stageHeight * 2;
		    self.redBG.crossline.scaleX = 0;
		    self.logoMC.scaleX = self.logoMC.scaleY = 0.5;
		    self.logoMC.x = 210;
		
		
		
		    // anim start
		
		    TweenMax.to(self.toplineMC, 0.75, {
		        y: 7,
		        ease: Expo.easeOut,
		        delay: d
		    });
		
		    //F1 text
		    TweenMax.to(self.t3MC, 0.85, {
		        y: 130,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.t2MC, 0.85, {
		        y: 130,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.t1MC, 0.85, {
		        y: 88,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		
		    //F1 Wings
		    for (var i = 0; i < 10; i++) {
		
		        TweenMax.to(self["wing" + i + "MC"], 3, {
		            y: wingToY[i],
		            ease: Elastic.easeOut.config(0.6, 0.6),
		            delay: 0.02 * i
		        });
		        TweenMax.to(self["wing" + i + "MC"], 3, {
		            y: wingToYBack,
		            ease: Elastic.easeInOut.config(0.5, 0.4),
		            delay: 1.2 + 0.01 * i
		        });
		
		    }
		
		    //F1 text out
		    TweenMax.to(self.toplineMC, 0.75, {
		        y: -50,
		        ease: Expo.easeInOut,
		        delay: 2.5
		    });
		
		    TweenMax.to([self.t3MC, self.t2MC, self.t1MC], 1.5, {
		        y: -stageHeight,
		        ease: Expo.easeInOut,
		        delay: 2
		    });
		    d -= 0.2;
		    TweenMax.delayedCall(d, animateInFrame2);
		}
		
		function animateInFrame2() {
		
		
		    var d = 0;
		
		    d += 1.75
		    TweenMax.to(self.redBG, 2, {
		        y: -250,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    // F2 text in 
		    d += 0.9;
		    TweenMax.to(self.redBG.v3MC, 0.85, {
		        x: 130,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.redBG.v4MC, 0.85, {
		        x: 190,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    //d += 0.05;
		    /*TweenMax.to(self.redBG.v2MC, 0.85, {
		        x: 150,
		        ease: Elastic.easeOut.config(0.5, 0.4),
		        delay: d
		    });
		    TweenMax.to(self.redBG.crossline, 0.5, {
		        x: self.redBG.crossline.x + 10,
		        scaleX: 1.0,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    d += 0.05;
		    TweenMax.to(self.redBG.v1MC, 0.85, {
		        x: 105,
		        ease: Elastic.easeOut.config(0.75, 0.4),
		        delay: d
		    });*/
		
		    d += 1.25;
		    TweenMax.delayedCall(d, animateInFrame3);
		}
		
		function animateInFrame3() {
		
		
		    var d = 0;
		
		    TweenMax.to(self.redBG, 2, {
		        y: -715,
		        ease: Expo.easeInOut,
		        delay: d
		    });
			 TweenMax.to(self.d1MC, 2, {
		        y: 45,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    TweenMax.to(self.logoMC, 1.0, {
		        y: 10,
		        ease: Sine.easeInOut,
		        delay: d
		    });
		
		    d += 1.11;
		   
		    TweenMax.to(self.d1MC, 0.8, {
		        scaleX: 0.8,
		        scaleY: 0.8,
		        ease: Elastic.easeOut.config(0.75, 0.4),
		        delay: d
		    });
		    /*TweenMax.to(self.legalMC, 0.5, {
		        alpha: 1.0,
		        ease: Circ.easeOut,
		        delay: d
		    });*/
			
			d += 0.1;
			TweenMax.to(self.nutMC, 1, {
		        y: 200,
		        ease: Bounce.easeOut,
		        delay: d
		    });
			d += 0.05;
			 TweenMax.to(self.fasirtMC, 1, {
		        y: 90,
		        ease: Bounce.easeOut,
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.friesMC, 1, {
		        y: 80,
		       ease: Bounce.easeOut,
		        delay: d
		    });
			TweenMax.to(self.strawberrySmallMC, 1, {
		        y: 210,
		       ease: Bounce.easeOut,
		        delay: d
		    });
			d +=0.05;
			TweenMax.to(self.vanillaMC, 1, {
		        y: 200,
		       ease: Bounce.easeOut,
		        delay: d
		    });
		    TweenMax.to(self.w1MC, 1.2, {
		        y: 80,
		        ease: Bounce.easeOut,
		        delay: d
		    });
		    d += 0.1;
		    TweenMax.to(self.burgerMC, 1, {
		        y: 100,
		        ease: Bounce.easeOut,
		        delay: d
		    });
		    
		    
			
		   TweenMax.delayedCall(d, animateInFrame4);
		
		
		}
		
		function animateInFrame4() {
		
		    var d = 1.5;
		
		
		
		    /*TweenMax.to(self.l1MC, 0.3, {
		        alpha: 1.0,
		        x: 70,
		        ease: Expo.easeInOut,
		        delay: d
		    });*/
		    TweenMax.to(self.l2MC, 0.3, {
		        alpha: 1.0,
		        x: 70,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    d += 0.2;
		    TweenMax.to(self.fasirtMC, 0.3, {
		        x: self.fasirtMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		    TweenMax.to(self.friesMC, 0.3, {
		        x: self.friesMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		    TweenMax.to(self.burgerMC, 0.3, {
		        x: self.burgerMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		    TweenMax.to(self.w1MC, 0.3, {
		        x: self.w1MC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
			TweenMax.to(self.nutMC, 0.3, {
		        x: self.nutMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
			TweenMax.to(self.vanillaMC, 0.3, {
		        x: self.vanillaMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
			TweenMax.to(self.strawberrySmallMC, 0.3, {
		        x: self.strawberrySmallMC.x + offsetX,
		        ease: Expo.easeOut,
		        delay: d
		    });
		    
		    d += 0.1;
		    TweenMax.to(self.bigShakeMC, 0.5, {
		        x: 220,
		        ease: Expo.easeInOut,
		        delay: d
		    });
			d += 0.1;
		    TweenMax.to(self.strawberryBigMC, 0.5, {
		        x: 210,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    TweenMax.to(self.l2MC, 0.5, {
		        x: 20,
		        ease: Expo.easeInOut,
		        delay: d
		    });
		    /*TweenMax.to(self.crossline2, 0.7, {
		        x: 20,
		        ease: Expo.easeInOut,
		        delay: d
		    });*/
			d += 0.5;
			TweenMax
			.to(self.ctaMC, 0.5, {
		        alpha: 1.0,
		        ease: Circ.easeOut,
		        delay: d
		    });
			d += 0.5;
			
			
			TweenMax.delayedCall(d, self.ctaMC.pina);
			
			
		
		
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1));

	// js
	this.nutMC = new lib.nutMC();
	this.nutMC.name = "nutMC";
	this.nutMC.parent = this;
	this.nutMC.setTransform(134.05,-342.4);

	this.strawberrySmallMC = new lib.strawberryBig();
	this.strawberrySmallMC.name = "strawberrySmallMC";
	this.strawberrySmallMC.parent = this;
	this.strawberrySmallMC.setTransform(210.2,-326.4,0.833,0.8333);

	this.strawberryBigMC = new lib.strawberryBig();
	this.strawberryBigMC.name = "strawberryBigMC";
	this.strawberryBigMC.parent = this;
	this.strawberryBigMC.setTransform(328,206);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.strawberryBigMC},{t:this.strawberrySmallMC},{t:this.nutMC}]}).wait(1));

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("A4/yvMAx/AAAMAAAAlfMgx/AAAg");
	this.shape.setTransform(160,120);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// f4
	this.burgerMC = new lib.burgerMC();
	this.burgerMC.name = "burgerMC";
	this.burgerMC.parent = this;
	this.burgerMC.setTransform(208.5,-440);

	this.vanillaMC = new lib.vanillaMC();
	this.vanillaMC.name = "vanillaMC";
	this.vanillaMC.parent = this;
	this.vanillaMC.setTransform(252.6,-348);

	this.fasirtMC = new lib.fasirtMC();
	this.fasirtMC.name = "fasirtMC";
	this.fasirtMC.parent = this;
	this.fasirtMC.setTransform(135,-450);

	this.d1MC = new lib.d1MC();
	this.d1MC.name = "d1MC";
	this.d1MC.parent = this;
	this.d1MC.setTransform(15,788.5);

	this.ctaMC = new lib.cta();
	this.ctaMC.name = "ctaMC";
	this.ctaMC.parent = this;
	this.ctaMC.setTransform(20,190);
	this.ctaMC.alpha = 0;

	this.crossline2 = new lib.crossline2();
	this.crossline2.name = "crossline2";
	this.crossline2.parent = this;
	this.crossline2.setTransform(-92,118.8);

	this.logoMC = new lib.kfclogo();
	this.logoMC.name = "logoMC";
	this.logoMC.parent = this;
	this.logoMC.setTransform(153,-93);

	this.w1MC = new lib.w1MC();
	this.w1MC.name = "w1MC";
	this.w1MC.parent = this;
	this.w1MC.setTransform(241.7,-460);

	this.friesMC = new lib.friesMC();
	this.friesMC.name = "friesMC";
	this.friesMC.parent = this;
	this.friesMC.setTransform(175.8,-460);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.friesMC},{t:this.w1MC},{t:this.logoMC},{t:this.crossline2},{t:this.ctaMC},{t:this.d1MC},{t:this.fasirtMC},{t:this.vanillaMC},{t:this.burgerMC}]}).wait(1));

	// f3
	this.l2MC = new lib.l2MC();
	this.l2MC.name = "l2MC";
	this.l2MC.parent = this;
	this.l2MC.setTransform(-141.45,83.8);
	this.l2MC.alpha = 0;

	this.l1MC = new lib.l1MC();
	this.l1MC.name = "l1MC";
	this.l1MC.parent = this;
	this.l1MC.setTransform(-92,102.8);

	this.legalMC = new lib.legalMC();
	this.legalMC.name = "legalMC";
	this.legalMC.parent = this;
	this.legalMC.setTransform(149.95,239.6,1,1,0,0,0,134.2,7.6);
	this.legalMC.alpha = 0;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.legalMC},{t:this.l1MC},{t:this.l2MC}]}).wait(1));

	// f1
	this.bigShakeMC = new lib.bigbigshake_1();
	this.bigShakeMC.name = "bigShakeMC";
	this.bigShakeMC.parent = this;
	this.bigShakeMC.setTransform(327.95,76);

	this.t3MC = new lib.t3mc();
	this.t3MC.name = "t3MC";
	this.t3MC.parent = this;
	this.t3MC.setTransform(240.65,-242.4,1,1,0,0,0,86,35.6);

	this.t2MC = new lib.t2mc();
	this.t2MC.name = "t2MC";
	this.t2MC.parent = this;
	this.t2MC.setTransform(92.55,-242.4,1,1,0,0,0,27.9,35.6);

	this.t1MC = new lib.t1mc();
	this.t1MC.name = "t1MC";
	this.t1MC.parent = this;
	this.t1MC.setTransform(180.2,-284.4,1,1,0,0,0,70.2,35.6);

	this.toplineMC = new lib.toplinemc();
	this.toplineMC.name = "toplineMC";
	this.toplineMC.parent = this;
	this.toplineMC.setTransform(80,-32.5,1,1,0,0,0,20,7.5);

	this.wing9MC = new lib.wing11();
	this.wing9MC.name = "wing9MC";
	this.wing9MC.parent = this;
	this.wing9MC.setTransform(318.2,-69.75,1,1,0,0,0,49.2,49.2);

	this.wing8MC = new lib.wing9();
	this.wing8MC.name = "wing8MC";
	this.wing8MC.parent = this;
	this.wing8MC.setTransform(244.7,-76.35,1,1,0,0,0,31.4,33.6);

	this.wing7MC = new lib.wing8();
	this.wing7MC.name = "wing7MC";
	this.wing7MC.parent = this;
	this.wing7MC.setTransform(236.3,-185.45,1,1,0,0,0,23,22.3);

	this.wing6MC = new lib.wing7();
	this.wing6MC.name = "wing6MC";
	this.wing6MC.parent = this;
	this.wing6MC.setTransform(146.55,-76.7,1,1,0,0,0,21.6,35.7);

	this.wing5MC = new lib.wing6();
	this.wing5MC.name = "wing5MC";
	this.wing5MC.parent = this;
	this.wing5MC.setTransform(29.85,-127.7,1,1,0,0,0,28.9,25.7);

	this.wing4MC = new lib.wing5();
	this.wing4MC.name = "wing4MC";
	this.wing4MC.parent = this;
	this.wing4MC.setTransform(53.95,-98.9,1,1,0,0,0,70,63.5);

	this.wing3MC = new lib.wing4();
	this.wing3MC.name = "wing3MC";
	this.wing3MC.parent = this;
	this.wing3MC.setTransform(340.95,-265.8,1,1,0,0,0,88,84.2);

	this.wing2MC = new lib.wing3();
	this.wing2MC.name = "wing2MC";
	this.wing2MC.parent = this;
	this.wing2MC.setTransform(199.55,-277.7,1,1,0,0,0,30.6,48.7);

	this.wing1MC = new lib.wing2();
	this.wing1MC.name = "wing1MC";
	this.wing1MC.parent = this;
	this.wing1MC.setTransform(78.4,-219.15,1,1,0,0,0,30.5,40.6);

	this.wing0MC = new lib.wing1();
	this.wing0MC.name = "wing0MC";
	this.wing0MC.parent = this;
	this.wing0MC.setTransform(55.2,-244.6,1,1,0,0,0,75.2,66);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.wing0MC},{t:this.wing1MC},{t:this.wing2MC},{t:this.wing3MC},{t:this.wing4MC},{t:this.wing5MC},{t:this.wing6MC},{t:this.wing7MC},{t:this.wing8MC},{t:this.wing9MC},{t:this.toplineMC},{t:this.t1MC},{t:this.t2MC},{t:this.t3MC},{t:this.bigShakeMC}]}).wait(1));

	// f2
	this.redBG = new lib.redBG();
	this.redBG.name = "redBG";
	this.redBG.parent = this;
	this.redBG.setTransform(150,365,1,1,0,0,0,150,125);

	this.timeline.addTween(cjs.Tween.get(this.redBG).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(18.6,-340,426.9,1680);
// library properties:
lib.properties = {
	id: '8D1598B6579E4EADB310E734EDB2EB4E',
	width: 320,
	height: 240,
	fps: 60,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/bigbigshake.png", id:"bigbigshake"},
		{src:"images/bigshake1.png", id:"bigshake1"},
		{src:"images/bigshake2.png", id:"bigshake2"},
		{src:"images/bigshake3.png", id:"bigshake3"},
		{src:"images/bigshake4.png", id:"bigshake4"},
		{src:"images/logo.png", id:"logo"},
		{src:"images/nut.png", id:"nut"},
		{src:"images/puppet.png", id:"puppet"},
		{src:"images/shake1.png", id:"shake1"},
		{src:"images/shake10.png", id:"shake10"},
		{src:"images/shake2.png", id:"shake2"},
		{src:"images/shake3.png", id:"shake3"},
		{src:"images/shake4.png", id:"shake4"},
		{src:"images/shake5.png", id:"shake5"},
		{src:"images/shake6.png", id:"shake6"},
		{src:"images/shake7.png", id:"shake7"},
		{src:"images/shake8.png", id:"shake8"},
		{src:"images/shake9.png", id:"shake9"},
		{src:"images/strawberry.png", id:"strawberry"},
		{src:"images/vanilla.png", id:"vanilla"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['8D1598B6579E4EADB310E734EDB2EB4E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}			
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;			
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});			
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;			
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;